//
//  ViewController.swift
//  PresenceChecker
//
//  Created by Benjamin Mecanovic on 02/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import Firebase

protocol LoginRegisterViewControllerDelegate {
    func loginInitiated()
}

class LoginRegisterViewController: UIViewController, UIScrollViewDelegate {
    
    var delegate: LoginRegisterViewControllerDelegate?
    
    let scrollView: UIScrollView = {
        let sv = UIScrollView()
        sv.translatesAutoresizingMaskIntoConstraints = false
        sv.isPagingEnabled = true
        return sv
    }()
    
    let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.translatesAutoresizingMaskIntoConstraints = false
        pc.numberOfPages = 2
        return pc
    }()
    
    let registerNameTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .darkGray
        tf.layer.borderWidth = 4
        tf.layer.borderColor = UIColor.white.cgColor
        tf.placeholder = "Enter name"
        tf.textAlignment = .center
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        return tf
    }()
    
    let registerEmailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .darkGray
        tf.layer.borderWidth = 4
        tf.layer.borderColor = UIColor.white.cgColor
        tf.placeholder = "Enter email"
        tf.textAlignment = .center
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        return tf
    }()
    
    let registerPasswordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .darkGray
        tf.layer.borderWidth = 4
        tf.layer.borderColor = UIColor.white.cgColor
        tf.isSecureTextEntry = true
        tf.placeholder = "Enter password"
        tf.textAlignment = .center
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        return tf
    }()
    
    let registerButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Register", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor(displayP3Red: 0.1, green: 0.1, blue: 0.1, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(handleRegisterButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let loginContainerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let loginEmailTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .darkGray
        tf.layer.borderWidth = 4
        tf.layer.borderColor = UIColor.white.cgColor
        tf.placeholder = "Enter email"
        tf.textAlignment = .center
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        return tf
    }()
    
    let loginPasswordTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.backgroundColor = .darkGray
        tf.layer.borderWidth = 4
        tf.isSecureTextEntry = true
        tf.layer.borderColor = UIColor.white.cgColor
        tf.placeholder = "Enter password"
        tf.textAlignment = .center
        tf.layer.cornerRadius = 10
        tf.clipsToBounds = true
        return tf
    }()
    
    let loginButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Login", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 18)
        button.setTitleColor(UIColor(displayP3Red: 0.1, green: 0.1, blue: 0.1, alpha: 1), for: .normal)
        button.layer.cornerRadius = 25
        button.clipsToBounds = true
        button.backgroundColor = .white
        button.addTarget(self, action: #selector(handleLoginButtonPressed), for: .touchUpInside)
        return button
    }()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(displayP3Red: 0.1, green: 0.1, blue: 0.1, alpha: 1)
        scrollView.delegate = self
        setupViews()
        setupToolbarDoneButton(textField: loginEmailTextField)
        setupToolbarDoneButton(textField: loginPasswordTextField)
        setupToolbarDoneButton(textField: registerNameTextField)
        setupToolbarDoneButton(textField: registerEmailTextField)
        setupToolbarDoneButton(textField: registerPasswordTextField)
        
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x / scrollView.frame.width == 0 {
            pageControl.currentPage = 0
        }
        else if scrollView.contentOffset.x / scrollView.frame.width == 1 {
            pageControl.currentPage = 1
        }
        
    }
    
    func setupToolbarDoneButton(textField: UITextField){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(removeKeyboard))
        toolbar.setItems([flexibleSpace,doneButton], animated: true)
        textField.inputAccessoryView = toolbar
        
    }
    
    @objc func removeKeyboard(){
        view.endEditing(true)
    }
    
    func setupViews() {
        setupScrollView()
        setupRegisterEmailTextField()
        setupRegisterNameTextField()
        setupRegisterPasswordTextField()
        setupLoginContainerView()
        setupLoginEmailTextField()
        setupLoginPasswordTextField()
        setupLoginButton()
        setupRegisterButton()
        setupPageControl()
    }

    func setupScrollView() {
        view.addSubview(scrollView)
        scrollView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
        scrollView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        scrollView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        
        scrollView.contentSize.width = 2 * view.frame.width
    }
    
    func setupRegisterEmailTextField() {
        scrollView.addSubview(registerEmailTextField)
        registerEmailTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: view.frame.width).isActive = true
        registerEmailTextField.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor).isActive = true
        registerEmailTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerEmailTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterNameTextField() {
        scrollView.addSubview(registerNameTextField)
        registerNameTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: view.frame.width).isActive = true
        registerNameTextField.bottomAnchor.constraint(equalTo: registerEmailTextField.topAnchor, constant: -5).isActive = true
        registerNameTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterPasswordTextField() {
        scrollView.addSubview(registerPasswordTextField)
        registerPasswordTextField.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: view.frame.width).isActive = true
        registerPasswordTextField.topAnchor.constraint(equalTo: registerEmailTextField.bottomAnchor, constant: 5).isActive = true
        registerPasswordTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        registerPasswordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupRegisterButton() {
        scrollView.addSubview(registerButton)
        registerButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor, constant: view.frame.width).isActive = true
        registerButton.topAnchor.constraint(equalTo: registerPasswordTextField.bottomAnchor, constant: 5).isActive = true
        registerButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        registerButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupLoginContainerView() {
        scrollView.addSubview(loginContainerView)
        loginContainerView.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        loginContainerView.centerYAnchor.constraint(equalTo: scrollView.centerYAnchor).isActive = true
        loginContainerView.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginContainerView.heightAnchor.constraint(equalToConstant: 105).isActive = true
    }
    
    func setupLoginEmailTextField() {
        loginContainerView.addSubview(loginEmailTextField)
        loginEmailTextField.topAnchor.constraint(equalTo: loginContainerView.topAnchor).isActive = true
        loginEmailTextField.centerXAnchor.constraint(equalTo: loginContainerView.centerXAnchor).isActive = true
        loginEmailTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginEmailTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupLoginPasswordTextField() {
        loginContainerView.addSubview(loginPasswordTextField)
        loginPasswordTextField.topAnchor.constraint(equalTo: loginEmailTextField.bottomAnchor, constant: 5).isActive = true
        loginPasswordTextField.centerXAnchor.constraint(equalTo: loginContainerView.centerXAnchor).isActive = true
        loginPasswordTextField.widthAnchor.constraint(equalToConstant: 250).isActive = true
        loginPasswordTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
    }
    
    func setupLoginButton() {
        scrollView.addSubview(loginButton)
        loginButton.centerXAnchor.constraint(equalTo: scrollView.centerXAnchor).isActive = true
        loginButton.topAnchor.constraint(equalTo: loginContainerView.bottomAnchor, constant: 5).isActive = true
        loginButton.widthAnchor.constraint(equalToConstant: 150).isActive = true
        loginButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    
    func setupPageControl() {
        view.addSubview(pageControl)
        pageControl.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        pageControl.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -50).isActive = true
        pageControl.widthAnchor.constraint(equalToConstant: 25).isActive = true
        pageControl.heightAnchor.constraint(equalToConstant: 10).isActive = true
        pageControl.currentPage = 0
    }
    
    @objc func handleRegisterButtonPressed() {
        guard let name = registerNameTextField.text ,let email = registerEmailTextField.text, let password = registerPasswordTextField.text else {
            print("Plese fill up all fields.")
            return
        }
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            if error != nil {
                print("Couldnt create user")
                return
            }
            guard let uid = user?.user.uid else {
                print("UID doesn't exist")
                return
            }
            let ref = Database.database().reference()
            let userReference = ref.child("students").child(uid)
            let userInfo: [String : String] = ["name" : name, "email" : email, "id" : uid]
            userReference.updateChildValues(userInfo, withCompletionBlock: { (error, ref) in
                if error != nil {
                    print("Couldn't update values on registration")
                }
                
            })
            self.delegate?.loginInitiated()
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    @objc func handleLoginButtonPressed() {
        guard let email = loginEmailTextField.text, let password = loginPasswordTextField.text else {
            print("Please fill all login info")
            return
        }
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if error != nil {
                print("Couldn't log you in")
                return
            }
            self.delegate?.loginInitiated()
            self.dismiss(animated: true, completion: nil)
            print("Sucessfully logged in")
        }
    }
}

