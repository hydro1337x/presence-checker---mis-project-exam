//
//  Subject.swift
//  PresenceChecker
//
//  Created by Benjamin Mecanovic on 05/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit

class Subject {
    public var id: String?
    public var name: String?
}
