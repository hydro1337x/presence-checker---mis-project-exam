//
//  PresenceDisplayViewController.swift
//  PresenceChecker
//
//  Created by Benjamin Mecanovic on 05/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import Firebase

class PresenceDisplayViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    var presenceValidationVC: PresenceValidationViewController?
    var subjectsArray = [Subject]();
    var datesArray = [CustomDate]();
    var studentsArray = [String]();
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let subjectPicker: UIPickerView = {
        let pv = UIPickerView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        pv.layer.cornerRadius = 10
        pv.layer.masksToBounds = true
        pv.backgroundColor = .white
        return pv
    }()
    
    let datePicker: UIPickerView = {
        let pv = UIPickerView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        pv.layer.cornerRadius = 10
        pv.layer.masksToBounds = true
        pv.backgroundColor = .gray
        pv.isUserInteractionEnabled = false
        return pv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
//        fetchSubjects()
        presenceValidationVC?.presenceDisplayVC = self
        view.backgroundColor = UIColor(displayP3Red: 0.1, green: 0.1, blue: 0.1, alpha: 1)
        setupContainerView()
        setupSubjectPickerView()
        setupDatePickerView()
        subjectPicker.delegate = self
        subjectPicker.dataSource = self
        datePicker.delegate = self
        datePicker.dataSource = self
        
    }
    
    func setupContainerView() {
        view.addSubview(containerView)
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerYAnchor).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 220).isActive = true
    }
    
    func setupSubjectPickerView() {
        view.addSubview(subjectPicker)
        subjectPicker.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        subjectPicker.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        subjectPicker.widthAnchor.constraint(equalToConstant: 200).isActive = true
        subjectPicker.heightAnchor.constraint(equalToConstant: 100).isActive  = true
    }
    
    func setupDatePickerView() {
        view.addSubview(datePicker)
        datePicker.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        datePicker.topAnchor.constraint(lessThanOrEqualTo: subjectPicker.bottomAnchor, constant: 20).isActive = true
        datePicker.widthAnchor.constraint(equalToConstant: 200).isActive = true
        datePicker.heightAnchor.constraint(equalToConstant: 100).isActive  = true
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == subjectPicker {
            return subjectsArray.count + 1
        }
        else {
            return datesArray.count + 1
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == subjectPicker && row != 0{
            return subjectsArray[row-1].name
        }
        else if pickerView == datePicker && row != 0 {
            return datesArray[row-1].date
        }
        else if pickerView == subjectPicker && row == 0 {
            return "Select subject"
        }
        else {
            return "Select date"
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == subjectPicker && row != 0 {
            datesArray.removeAll()
            datePicker.isUserInteractionEnabled = true
            datePicker.backgroundColor = .white
            
            if let subjectID = subjectsArray[row-1].id {
                fetchDates(subjectID: subjectID)
            }
        }
        else if pickerView == datePicker && row != 0 && subjectPicker.selectedRow(inComponent: 0) != 0 {
            let dateID = datesArray[row-1].id
            let date = datesArray[row-1].date
            if let subjectID = subjectsArray[subjectPicker.selectedRow(inComponent: 0)-1].id {
                Database.database().reference().child("presence").child(subjectID).child(dateID).child("students").observe(.value) { (snapshot) in
                    if let dictionary = snapshot.value as? [String : AnyObject] {
                        if dictionary[(Auth.auth().currentUser?.uid)!] != nil {
                            let alertController = UIAlertController(title: "Presence", message: "You were present on \(date)", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(action)
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                            let alertController = UIAlertController(title: "Presence", message: "You were NOT present on \(date)", preferredStyle: .alert)
                            let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                            alertController.addAction(action)
                            self.present(alertController, animated: true, completion: nil)
                        }
                    }
                    else {
                        let alertController = UIAlertController(title: "Presence", message: "You were NOT present on \(date)", preferredStyle: .alert)
                        let action = UIAlertAction(title: "OK", style: .default, handler: nil)
                        alertController.addAction(action)
                        self.present(alertController, animated: true, completion: nil)
                    }
                }
            }
            subjectPicker.selectRow(0, inComponent: 0, animated: true)
            datePicker.selectRow(0, inComponent: 0, animated: true)
            datePicker.isUserInteractionEnabled = false
            datePicker.backgroundColor = .gray
        }
        
    }
    
    func fetchSubjects() {
        Database.database().reference().child("subjects").observe(.childAdded) { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                if let name = dictionary["name"] as? String, let id = dictionary["id"] as? String {
                    let subject = Subject()
                    subject.id = id
                    subject.name = name
                    self.subjectsArray.append(subject)
                    DispatchQueue.main.async {
                        self.subjectPicker.reloadAllComponents()
                    }
                }
            }
        }
    }
    
    func fetchDates(subjectID: String) {
        Database.database().reference().child("presence").child(subjectID).observe(.childAdded) { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                if let firDate = dictionary["date"] as? String, let firDateId = dictionary["id"] as? String {
                    
                    let date = CustomDate(id: firDateId, date: firDate)
                    self.datesArray.append(date)
                    DispatchQueue.main.async {
                        self.datePicker.reloadAllComponents()
                    }
                    
                }
            }
        }
    }

}
