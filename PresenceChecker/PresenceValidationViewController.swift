//
//  PresenceValidationViewController.swift
//  PresenceChecker
//
//  Created by Benjamin Mecanovic on 03/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation



class PresenceValidationViewController: UIViewController, LoginRegisterViewControllerDelegate, CLLocationManagerDelegate, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    
    
    
    let loginRegisterVC = LoginRegisterViewController()
    var presenceDisplayVC = PresenceDisplayViewController()
    var locationManager: CLLocationManager?
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    var subjectsArray = [Subject]()
    
    
    let containerView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    
    let tokenTextField: UITextField = {
        let tf = UITextField()
        tf.translatesAutoresizingMaskIntoConstraints = false
        tf.placeholder = "Enter token"
        tf.textAlignment = .center
        tf.backgroundColor = .darkGray
        tf.layer.borderColor = UIColor.white.cgColor
        tf.layer.borderWidth = 3
        tf.layer.cornerRadius = 10
        tf.layer.masksToBounds = true
        return tf
    }()
    
    let submitButton: UIButton = {
        let button = UIButton(type: .system)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Submit", for: .normal)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 16)
        button.setTitleColor(.black, for: .normal)
        button.backgroundColor = .darkGray
        button.layer.cornerRadius = 20
        button.layer.borderColor = UIColor.white.cgColor
        button.layer.borderWidth = 3
        button.layer.masksToBounds = true
        button.addTarget(self, action: #selector(handleSubmitButtonPressed), for: .touchUpInside)
        return button
    }()
    
    let segmentedControlView: UISegmentedControl = {
        let sgControl = UISegmentedControl()
        sgControl.translatesAutoresizingMaskIntoConstraints = false
        sgControl.insertSegment(withTitle: "Main", at: 0, animated: false)
        sgControl.insertSegment(withTitle: "Campus", at: 1, animated: false)
        sgControl.tintColor = .white
        sgControl.selectedSegmentIndex = 0
        return sgControl
    }()
    
    let pickerView: UIPickerView = {
        let pv = UIPickerView()
        pv.translatesAutoresizingMaskIntoConstraints = false
        pv.layer.cornerRadius = 10
        pv.layer.masksToBounds = true
        pv.backgroundColor = .white
        return pv
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor(displayP3Red: 0.1, green: 0.1, blue: 0.1, alpha: 1)
        loginRegisterVC.delegate = self
        presenceDisplayVC.presenceValidationVC = self
        setupViews()
        setupToolbarDoneButton(textField: tokenTextField)
        pickerView.dataSource = self
        pickerView.delegate = self
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Logout", style: .plain, target: self, action: #selector(handleLogout))
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .bookmarks, target: self, action: #selector(handleDisplayPresence))
        checkIfLoggedIn()
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestWhenInUseAuthorization()
        locationManager?.startUpdatingLocation()
        fetchSubjects()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return subjectsArray.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return subjectsArray[row].name
    }
    
    func setupToolbarDoneButton(textField: UITextField){
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(removeKeyboard))
        toolbar.setItems([flexibleSpace,doneButton], animated: true)
        textField.inputAccessoryView = toolbar
        
    }
    
    @objc func removeKeyboard(){
        view.endEditing(true)
    }
    
    func setupViews() {
        setupContainerView()
        setupSegmentedControlView()
        setupPickerView()
        setupTokenTextField()
        setupSubmitButton()
    }
    
    func setupContainerView() {
        view.addSubview(containerView)
        containerView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        containerView.centerYAnchor.constraint(equalTo: view.centerYAnchor).isActive = true
        containerView.widthAnchor.constraint(equalToConstant: view.frame.width).isActive = true
        containerView.heightAnchor.constraint(equalToConstant: 350).isActive = true
    }
    
    func setupSegmentedControlView() {
        containerView.addSubview(segmentedControlView)
        segmentedControlView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        segmentedControlView.topAnchor.constraint(equalTo: containerView.topAnchor).isActive = true
        segmentedControlView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        segmentedControlView.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func setupPickerView() {
        containerView.addSubview(pickerView)
        pickerView.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        pickerView.topAnchor.constraint(equalTo: segmentedControlView.bottomAnchor, constant: 10).isActive = true
        pickerView.widthAnchor.constraint(equalToConstant: 200).isActive = true
        pickerView.heightAnchor.constraint(equalToConstant: 200).isActive = true
    }
    
    func setupTokenTextField() {
        containerView.addSubview(tokenTextField)
        tokenTextField.topAnchor.constraint(equalTo: pickerView.bottomAnchor, constant: 10).isActive = true
        tokenTextField.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        tokenTextField.widthAnchor.constraint(equalToConstant: 200).isActive = true
        tokenTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func setupSubmitButton() {
        containerView.addSubview(submitButton)
        submitButton.topAnchor.constraint(equalTo: tokenTextField.bottomAnchor, constant: 10).isActive = true
        submitButton.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        submitButton.widthAnchor.constraint(equalToConstant: 200).isActive = true
        submitButton.heightAnchor.constraint(equalToConstant: 40).isActive = true
    }
    
    func checkIfLoggedIn() {
        if Auth.auth().currentUser?.uid == nil {
            perform(#selector(handleLogout), with: nil, afterDelay: 0)
        } else {
            fetchUserInfo()
            locationManager?.startUpdatingLocation()
        }
    }
    
    func fetchUserInfo() {
        if let uid = Auth.auth().currentUser?.uid {
            let ref = Database.database().reference()
            ref.child("students").child(uid).observeSingleEvent(of: .value, with: { (snapshot) in
                print(snapshot)
                if let dictionary = snapshot.value as? [String : AnyObject] {
                    print(dictionary)
                    self.navigationItem.title = dictionary["name"] as? String
                }
            }, withCancel: nil)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations[locations.count-1]
        if location.horizontalAccuracy > 0 {
            
            longitude = location.coordinate.longitude
            latitude = location.coordinate.latitude
        }
        
        
    }
    
    @objc func handleLogout() {
        do {
            try Auth.auth().signOut()
        }
        catch let logoutError{
            print(logoutError)
        }
        present(loginRegisterVC, animated: true, completion: nil)
    }
    
    @objc func handleSubmitButtonPressed() {
        if segmentedControlView.selectedSegmentIndex == 0 && tokenTextField.text != "" {
            Database.database().reference().child("locations").child("main").observe(.value) { (snapshot) in
                if let coordinates = snapshot.value as? [String : AnyObject] {
                    if let minLat = coordinates["lat"]?["min"] as? CLLocationDegrees, let maxLat = coordinates["lat"]?["max"] as? CLLocationDegrees, let minLon = coordinates["lon"]?["min"] as? CLLocationDegrees, let maxLon = coordinates["lon"]?["max"] as? CLLocationDegrees {
                        if let userCoordinates = self.locationManager?.location?.coordinate {
                            print(userCoordinates)
                            let userLat = userCoordinates.latitude
                            let userLon = userCoordinates.longitude
                            if userLat >= minLat && userLon >= minLon && userLat <= maxLat && userLon <= maxLon {
                                
                                if let subjectId = self.subjectsArray[self.pickerView.selectedRow(inComponent: 0)].id {
                                    Database.database().reference().child("subjects").child(subjectId).observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let dictionary = snapshot.value as? [String : AnyObject] {
                                            if let token = dictionary["token"] as? String, let building = dictionary["location"] as? String {
                                                if token == self.tokenTextField.text && building == "main" {
                                                    Database.database().reference().child("presence").child(subjectId).observe(.childAdded, with: { (snapshot) in
                                                        if let dictionary = snapshot.value as? [String : AnyObject]
                                                        {
                                                            let D = Date()
                                                            let formatter = DateFormatter()
                                                            formatter.dateFormat = "d.M.yyyy."
                                                            let currentDate = formatter.string(from: D)
                                                            if let date = dictionary["date"] as? String, let dateId = dictionary["id"] as? String {
                                                                print(date, dateId)
                                                                print(currentDate)
                                                                if date == currentDate {
                                                                    if let uid = Auth.auth().currentUser?.uid {
                                                                        let values: [String : String] = [uid : uid]
                                                                        Database.database().reference().child("presence").child(subjectId).child(dateId).child("students").updateChildValues(values, withCompletionBlock: { (err, ref) in
                                                                            if err != nil {
                                                                                print("Couldnt add student to presence node")
                                                                            }
                                                                            else {
                                                                                let alert = UIAlertController(title: "Presence validation", message: "You successfully validated your presence!", preferredStyle: .alert)
                                                                                let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                                                                                alert.addAction(action)
                                                                                self.present(alert, animated: true, completion: nil)
                                                                                print("Successfully added student to presence node")
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })
                                                    print("Presence validated")
                                                }
                                                else {
                                                    print("Presence not valid")
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    print("Subject id is nil")
                                }
                            }
                        }
                    } else {
                        print("Problem with casting location data to CLLocationDegree")
                    }
                } else {
                    print("Problem with getting location data")
                }
            }
            
        } else if segmentedControlView.selectedSegmentIndex == 1 && tokenTextField.text != "" {
            Database.database().reference().child("locations").child("campus").observe(.value) { (snapshot) in
                if let coordinates = snapshot.value as? [String : AnyObject] {
                    if let minLat = coordinates["lat"]?["min"] as? CLLocationDegrees, let maxLat = coordinates["lat"]?["max"] as? CLLocationDegrees, let minLon = coordinates["lon"]?["min"] as? CLLocationDegrees, let maxLon = coordinates["lon"]?["max"] as? CLLocationDegrees {
                        if let userCoordinates = self.locationManager?.location?.coordinate {
                            print(userCoordinates)
                            let userLat = userCoordinates.latitude
                            let userLon = userCoordinates.longitude
                            if userLat >= minLat && userLon >= minLon && userLat <= maxLat && userLon <= maxLon {
                                
                                if let subjectId = self.subjectsArray[self.pickerView.selectedRow(inComponent: 0)].id {
                                    Database.database().reference().child("subjects").child(subjectId).observeSingleEvent(of: .value, with: { (snapshot) in
                                        if let dictionary = snapshot.value as? [String : AnyObject] {
                                            if let token = dictionary["token"] as? String, let building = dictionary["location"] as? String {
                                                if token == self.tokenTextField.text && building == "campus" {
                                                    Database.database().reference().child("presence").child(subjectId).observe(.childAdded, with: { (snapshot) in
                                                        if let dictionary = snapshot.value as? [String : AnyObject]
                                                        {
                                                            let D = Date()
                                                            let formatter = DateFormatter()
                                                            formatter.dateFormat = "d.M.yyyy."
                                                            let currentDate = formatter.string(from: D)
                                                            if let date = dictionary["date"] as? String, let dateId = dictionary["id"] as? String {
                                                                print(date, dateId)
                                                                print(currentDate)
                                                                if date == currentDate {
                                                                    if let uid = Auth.auth().currentUser?.uid {
                                                                        let values: [String : String] = [uid : uid]
                                                                        Database.database().reference().child("presence").child(subjectId).child(dateId).child("students").updateChildValues(values, withCompletionBlock: { (err, ref) in
                                                                            if err != nil {
                                                                                print("Couldnt add student to presence node")
                                                                            }
                                                                            else {
                                                                                print("Successfully added student to presence node")
                                                                            }
                                                                        })
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    })
                                                    print("Presence validated")
                                                }
                                                else {
                                                    print("Presence not valid")
                                                }
                                            }
                                        }
                                    })
                                }
                                else {
                                    print("Subject id is nil")
                                }
                            }
                        }
                    } else {
                        print("Problem with casting location data to CLLocationDegree")
                    }
                } else {
                    print("Problem with getting location data")
                }
            }
        }
            
    }
    
    @objc func handleDisplayPresence() {
        presenceDisplayVC.datesArray.removeAll()
        presenceDisplayVC.subjectsArray.removeAll()
        presenceDisplayVC.studentsArray.removeAll()
        presenceDisplayVC.datePicker.reloadAllComponents()
        presenceDisplayVC.subjectPicker.reloadAllComponents()
        presenceDisplayVC.fetchSubjects()
        navigationController?.pushViewController(presenceDisplayVC, animated: true)
        
    }
    
    func loginInitiated() {
        fetchUserInfo()
    }
    
    func fetchSubjects() {
        let ref = Database.database().reference()
        ref.child("subjects").observe(.childAdded) { (snapshot) in
            if let dictionary = snapshot.value as? [String : AnyObject] {
                let subject = Subject()
                subject.id = dictionary["id"] as? String
                subject.name = dictionary["name"] as? String
                self.subjectsArray.append(subject)
                DispatchQueue.main.async {
                    self.pickerView.reloadAllComponents()
                }
            }
        }
    }
}
