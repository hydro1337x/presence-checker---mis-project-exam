//
//  CustomDate.swift
//  PresenceChecker
//
//  Created by Benjamin Mecanovic on 06/05/2019.
//  Copyright © 2019 hydro1337x. All rights reserved.
//

import Foundation

class CustomDate {
    public var id: String
    public var date: String
    
    init(id: String, date: String) {
        self.date = date
        self.id = id
    }
}
